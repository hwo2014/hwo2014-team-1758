var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

var pista=-1;

var orden = {
    
    establecePotencia: function(potencia){
        return {msgType: "throttle",data: potencia};
    },
        
    cambiaCarril: function(direccion){
        if(direccion=="derecha") return cambiaCarrilDerecha();
        if(direccion=="izquierda") return cambiaCarrilIzquierda();
    },
        
    cambiaCarrilDerecha: function(){
        return {msgType: "switchLane", data: "Right"};
    },
    
    cambiaCarrilIzquierda: function(){
        return {msgType: "switchLane", data: "Left"};
    }
};

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
});

function send(json) {
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
  if (data.msgType === 'carPositions') {
    var pistaActual = data.data[0].piecePosition.pieceIndex;
    if(pistaActual != pista){
        if(pistaActual == 2){
            console.log('cambiaDeCarril');
            send(orden.cambiaCarrilDerecha());
        };
        console.log('Pista: ' + pistaActual);
        pista=pistaActual;
    };
    //console.log(JSON.stringify(data));
    send(orden.establecePotencia(0.67));
  } else {
    if (data.msgType === 'join') {
      console.log('Joined');
        console.log(JSON.stringify(data));
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } else if (data.msgType === 'gameInit') {      
        console.log('gameInit');
        //console.log(JSON.stringify(data));
	}
    send({
      msgType: "ping",
      data: {}
    });
  }
});

jsonStream.on('error', function() {
  return console.log("disconnected");
});


